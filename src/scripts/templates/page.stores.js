import Swiper from 'swiper/bundle';
import {addForegroundClass, breaks} from '../components/utils';

const desktop = window.matchMedia(`(min-width: ${breaks.medium}px)`);

const swiperStores = new Swiper('[data-role="store-swiper"]', {
  loop: true,
  speed: 1000,
  effect: 'fade',
  fadeEffect: {
    crossFade: true,
  },
  navigation: {
    nextEl: '.slider__arrow-right',
    prevEl: '.slider__arrow-left',
  },
  on: {
    beforeInit(sw) {
      if (desktop.matches) {
        addForegroundClass(sw);
      } else {
        document.querySelector('body').classList.remove('white-foreground');
      }
    },
    beforeTransitionStart(sw) {
      if (desktop.matches) {
        addForegroundClass(sw);
      } else {
        document.querySelector('body').classList.remove('white-foreground');
      }
    },
  },
});
