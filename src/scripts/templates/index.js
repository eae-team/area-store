import {load} from '@shopify/theme-sections';
import '../sections/product';
import '../sections/home-video';
import '../sections/home-slider';
import '../sections/home-slider-mobile';

load('*');
