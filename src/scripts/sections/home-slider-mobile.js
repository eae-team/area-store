import Swiper from 'swiper/bundle';
import {addForegroundClass, breaks} from '../components/utils';
// elements
const swiperHomeMobile = document.querySelector('[data-role="home-slider-mobile"]');
const mutedBtnEl = document.querySelector('[data-action="btn-muted"]');

const breakpointMobile = window.matchMedia(`(max-width: ${breaks.medium}px)`);
let swiperInstanceMobile;

// swiper instance
function swiperMobile() {
  const autoPlay = swiperHomeMobile.dataset.auto * 1000;
  let autoPlayOption = false;
  if (autoPlay) {
    autoPlayOption = {
      delay: autoPlay,
    };
  }

  swiperInstanceMobile = new Swiper(swiperHomeMobile, {
    loop: true,
    speed: 1000,
    effect: 'fade',
    autoplay: autoPlayOption,
    fadeEffect: {
      crossFade: true,
    },
    navigation: {
      nextEl: '.slider__arrow-right',
      prevEl: '.slider__arrow-left',
    },
    on: {
      beforeInit(sw) {
        addForegroundClass(sw);
      },
      beforeTransitionStart(sw) {
        addForegroundClass(sw);
      },
      slideChange(sw) {
        checkSlide(sw);
      },
    },
  });
}

function checkSlide(sw) {
  if (!mutedBtnEl) {
    return;
  }
  const currentSlide = sw.slides[sw.activeIndex];

  if (currentSlide.classList.contains('slide-inner-video')) {
    mutedBtnEl.classList.add('home-slider--show');
    mutedBtnEl.addEventListener('click', () => mutedToogle(currentSlide));
  } else {
    mutedBtnEl.classList.remove('home-slider--show');
    mutedBtnEl.removeEventListener('click', mutedToogle);
    mutedAllVideos();
  }
}

function breakpointChecker() {
  if (breakpointMobile.matches === false) {
    if (swiperInstanceMobile !== undefined) {
      swiperInstanceMobile.destroy(true, true);
    }
  } else if (breakpointMobile.matches === true && swiperHomeMobile) {
    swiperMobile();
  }
}

function mutedToogle(currentSlide) {
  const video = currentSlide.querySelector('video');
  video.muted = !video.muted;
}

function mutedAllVideos() {
  const videos = swiperHomeMobile.querySelectorAll('video');
  videos.forEach((element) => {
    element.muted = true;
  });
}

breakpointMobile.addListener(breakpointChecker);
breakpointChecker();
