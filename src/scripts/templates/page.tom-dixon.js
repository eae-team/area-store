import { on } from 'delegated-events';
import Swiper from 'swiper/bundle';

const video1 = document.querySelector('[data-role="video-1"]');
const video1preview = document.querySelector('[data-role="video-wide-preview-1"]');
const video1PlayIcon = document.querySelector('[data-role="video-play-icon-1"]');
const video2 = document.querySelector('[data-role="video-2"]');
const video2preview = document.querySelector('[data-role="video-wide-preview-2"]');
const video2PlayIcon = document.querySelector('[data-role="video-play-icon-2"]');
const video2Container = document.querySelector('[data-role="video-2-container"]');
const slider1 = document.querySelector('[data-role="slider1"]');
const slider2 = document.querySelector('[data-role="slider2"]');
const slideEls = document.querySelectorAll('[data-role="slide"]');

let swiperSlider1;
let swiperSlider2;

function startFullscreen(videoEl) {
  if (videoEl.requestFullscreen) {
    videoEl.requestFullscreen();
  } else if (videoEl.mozRequestFullScreen) {
    videoEl.mozRequestFullScreen();
  } else if (videoEl.webkitRequestFullscreen) {
    videoEl.webkitRequestFullscreen();
  } else if (videoEl.msRequestFullscreen) {
    videoEl.msRequestFullscreen();
  }
}

function endFullscreen() {
  if (document.exitFullscreen) {
    document.exitFullscreen();
  } else if (document.mozCancelFullScreen) {
    document.mozCancelFullScreen();
  } else if (document.webkitExitFullscreen) {
    document.webkitExitFullscreen();
  } else if (document.msExitFullscreen) {
    document.msExitFullscreen();
  }
}

if (video1) {
  on('click', '[data-action="sound-video1"]', (event) => {
    event.preventDefault();
    if (video1) {
      video1.muted = !video1.muted;
    }
    event.target.classList.toggle('tomd--video-btn-sound-off');
  });

  on('click', '[data-action="fullscreen-video1"]', (event) => {
    event.preventDefault();
    if (document.fullscreenElement) {
      endFullscreen();
    } else {
      startFullscreen(video1);
    }
  });

  on('click', '[data-role="video-1"]', (event) => {
    event.preventDefault();
    if (video1preview) {
      if (!video1preview.classList.contains('tomd--video-wide-preview-hide')) {
        video1preview.classList.add('tomd--video-wide-preview-hide');
      }
    }

    if (document.fullscreenElement === video1 ||
      document.webkitFullscreenElement === video1 ||
      document.mozFullScreenElement === video1 ||
      document.msFullscreenElement === video1) {
      return;
    }

    if (video1.paused) {
      video1.play();
      video1PlayIcon.classList.add('tomd--video-play-icon-hide');
    } else {
      video1.pause();
      video1PlayIcon.classList.remove('tomd--video-play-icon-hide');
    }
  });

  video1.addEventListener('ended', () => {
    if (video1preview) {
      video1preview.classList.remove('tomd--video-wide-preview-hide');
      video1PlayIcon.classList.remove('tomd--video-play-icon-hide');
    }
  });

  video1.addEventListener('play', () => {
    video1PlayIcon.classList.add('tomd--video-play-icon-hide');
  });

  video1.addEventListener('pause', () => {
    video1PlayIcon.classList.remove('tomd--video-play-icon-hide');
  });
}

if (video2) {
  on('click', '[data-action="sound-video2"]', (event) => {
    event.preventDefault();
    if (video2) {
      video2.muted = !video2.muted;
    }
    event.target.classList.toggle('tomd--video-btn-sound-off');
  });

  on('click', '[data-action="fullscreen-video2"]', (event) => {
    event.preventDefault();
    if (document.fullscreenElement) {
      endFullscreen();
    } else {
      startFullscreen(video2);
    }
  });

  on('click', '[data-role="video-2"]', (event) => {
    event.preventDefault();
    if (video2preview) {
      if (!video2preview.classList.contains('tomd--video-wide-preview-hide')) {
        video2preview.classList.add('tomd--video-wide-preview-hide');
      }
    }

    if (document.fullscreenElement === video2 ||
      document.webkitFullscreenElement === video2 ||
      document.mozFullScreenElement === video2 ||
      document.msFullscreenElement === video2) {
      return;
    }

    if (video2.paused) {
      video2.play();
    } else {
      video2.pause();
    }
  });

  video2.addEventListener('ended', () => {
    if (video2preview) {
      video2preview.classList.remove('tomd--video-wide-preview-hide');
      video2PlayIcon.classList.remove('tomd--video-play-icon-hide');
    }
  });

  video2.addEventListener('play', () => {
    video2PlayIcon.classList.add('tomd--video-play-icon-hide');
  });

  video2.addEventListener('pause', () => {
    video2PlayIcon.classList.remove('tomd--video-play-icon-hide');
  });
}

document.addEventListener('fullscreenchange', () => {
  if (video1) {
    if (document.fullscreenElement === video1 ||
      document.webkitFullscreenElement === video1 ||
      document.mozFullScreenElement === video1 ||
      document.msFullscreenElement === video1) {
      video1.classList.add('tomd--remove-cursor');
    } else {
      video1.classList.remove('tomd--remove-cursor');
    }
  }

  if (video2) {
    if (document.fullscreenElement === video2 ||
      document.webkitFullscreenElement === video2 ||
      document.mozFullScreenElement === video2 ||
      document.msFullscreenElement === video2) {
      video2Container.classList.add('tomd--remove-cursor');
    } else {
      video2Container.classList.remove('tomd--remove-cursor');
    }
  }
});

if (slider1) {
  swiperSlider1 = new Swiper(slider1, {
    spaceBetween: 5,
    slidesPerView: 'auto',
    speed: 1000,
    updateOnWindowResize: true,
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    pagination: {
      el: '.swiper-pagination',
      type: 'bullets',
    },
  });
}

if (slider2) {
  swiperSlider2 = new Swiper(slider2, {
    spaceBetween: 5,
    slidesPerView: 'auto',
    speed: 1000,
    updateOnWindowResize: true,
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    pagination: {
      el: '.swiper-pagination',
      type: 'bullets',
    },
  });
}

const resizeElements = () => {
  const viewportWidth = window.innerWidth * 0.8;

  let maxWidth = 0;
  let maxHeight = 0;

  slideEls.forEach((element) => {
    const width = parseInt(element.dataset.width, 10);
    const height = parseInt(element.dataset.height, 10);

    if (width > maxWidth) {
      maxWidth = width;
      maxHeight = height;
    }
  });

  const targetHeight = viewportWidth * (maxHeight / maxWidth);

  slideEls.forEach((element) => {
    const width = parseInt(element.dataset.width, 10);
    const height = parseInt(element.dataset.height, 10);

    const newWidth = targetHeight * (width / height);
    const newHeight = targetHeight;

    element.style.width = `${newWidth}px`;
    element.style.height = `${newHeight}px`;
  });

  if (swiperSlider1) {
    swiperSlider1.update();
  }

  if (swiperSlider2) {
    swiperSlider2.update();
  }
};

window.addEventListener('DOMContentLoaded', resizeElements);
window.addEventListener('resize', resizeElements);
