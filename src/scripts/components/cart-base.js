import {on} from 'delegated-events';
import {updateItem, closeCart} from '../components/cart-actions';

export class CartBase {
  constructor(cartEl, id) {
    this.el = cartEl;
    this.el.id = id;
    this.quantityEl = this.el.querySelector('[data-role="cart-quantity"]');
    this.totalEl = this.el.querySelector('[data-role="cart-total"]');
    this.subtotalEl = this.el.querySelector('[data-role="cart-subtotal"]');
    this.discountEl = this.el.querySelector('[data-role="cart-discount"]');
    // this.discountPercEl = this.el.querySelector('[data-role="cart-discount-perc"]');
    this.discountLineEl = this.el.querySelector('[data-role="cart-discount-line"]');
    this.itemsEl = this.el.querySelector('[data-role="cart-items"]');
    this.promoButtonEl = this.el.querySelector('[data-role="promo-button"]');

    on('click', `#${this.el.id} [data-action="decrease"]`, (event) => {
      const itemEl = event.target.closest('[data-line]');
      const lineId = itemEl.dataset.line;
      const quantity = Number(itemEl.querySelector('[name="quantity"]').value) - 1;
      updateItem(lineId, quantity);
    });

    on('click', `#${this.el.id} [data-action="increase"]`, (event) => {
      const itemEl = event.target.closest('[data-line]');
      const lineId = itemEl.dataset.line;
      const quantity = Number(itemEl.querySelector('[name="quantity"]').value) + 1;
      updateItem(lineId, quantity);
    });

    on('click',  `#${this.el.id} [data-action="close"]`, (event) => {
      event.preventDefault();
      closeCart();
    });
  }

  updateCart(cartData) {
    this.quantityEl.innerHTML = cartData.cartCount;
    this.totalEl.innerHTML = cartData.cartTotal;
    this.subtotalEl.innerHTML = cartData.cartSubtotal;
    this.itemsEl.innerHTML = cartData.cartItems;

    if (cartData.cartDiscount) {
      // this.discountPercEl.innerHTML = cartData.cartDiscountPerc;
      this.discountEl.innerHTML = cartData.cartDiscount;
      this.discountLineEl.style.display = 'flex';
    } else {
      this.discountLineEl.style.display = 'none';
    }

    if (cartData.cartItemCount === 0) {
      this.el.classList.add('js-empty-cart');
    } else {
      this.el.classList.remove('js-empty-cart');
    }

    if (this.promoButtonEl) {
      this.promoButtonEl.style.display = cartData.showPromoButton ? 'block' : 'none';
    }
  }
}
