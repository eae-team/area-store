import { on } from 'delegated-events';
import { breaks } from '../components/utils';

const homeVideo = document.querySelector('[data-role="home-video"]');

if (homeVideo) {
  const videoMobile = homeVideo.querySelector('[data-role="video-mobile"]');
  const videoDesktop = homeVideo.querySelector('[data-role="video-desktop"]');
  const endedMobile = homeVideo.querySelector('[data-role="ended-mobile"]');
  const endedDesktop = homeVideo.querySelector('[data-role="ended-desktop"]');
  const soundMobile = homeVideo.querySelector('[data-action="sound-mobile"]');
  const soundDesktop = homeVideo.querySelector('[data-action="sound-desktop"]');
  let flagDesktop = true;
  let flagMobile = true;

  let timer = null;

  function resizeManage() {
    if (window.innerWidth > breaks.medium) {
      // Desktop
      videoMobile.pause();
      if (videoDesktop.currentTime !== videoDesktop.duration) {
        videoDesktop.play();
      }
    } else if (window.innerWidth <= breaks.medium) {
      // Mobile
      videoDesktop.pause();
      if (videoMobile.currentTime !== videoMobile.duration) {
        videoMobile.play();
      }
    }
  }

  function videoMobileEnded() {
    const percent = (videoMobile.currentTime / videoMobile.duration) * 100;
    if (percent >= 70 && flagMobile) {
      flagMobile = false;
      endedMobile.classList.add('home-video--video-ended-show');
      soundMobile.classList.add('home-video--btn-hide');
    }
  }

  function videoDesktopEnded() {
    const percent = (videoDesktop.currentTime / videoDesktop.duration) * 100;
    if (percent >= 70 && flagDesktop) {
      flagDesktop = false;
      endedDesktop.classList.add('home-video--video-ended-show');
      soundDesktop.classList.add('home-video--btn-hide');
    }
  }

  window.addEventListener('resize', resizeManage);

  if (videoMobile && videoDesktop) {
    videoMobile.addEventListener('timeupdate', videoMobileEnded);
    videoDesktop.addEventListener('timeupdate', videoDesktopEnded);
  }

  on('click', '[data-action="replay-mobile"]', (event) => {
    event.preventDefault();
    endedMobile.classList.remove('home-video--video-ended-show');
    videoMobile.load();
    videoMobile.play();
    videoMobile.muted = false;
    soundMobile.classList.add('home-video--video-desktop-btn-sound-on');
    soundMobile.classList.remove('home-video--btn-hide');
    flagMobile = true;
  });

  on('click', '[data-action="replay-desktop"]', (event) => {
    event.preventDefault();
    endedDesktop.classList.remove('home-video--video-ended-show');
    videoDesktop.load();
    videoDesktop.play();
    videoDesktop.muted = false;
    soundDesktop.classList.add('home-video--video-desktop-btn-sound-on');
    soundDesktop.classList.remove('home-video--btn-hide');
    flagDesktop = true;
  });

  on('click', '[data-action="sound-mobile"]', (event) => {
    event.preventDefault();
    if (videoMobile) {
      videoMobile.muted = !videoMobile.muted;
    }
    event.target.classList.toggle('home-video--video-desktop-btn-sound-on');
  });

  on('click', '[data-action="sound-desktop"]', (event) => {
    event.preventDefault();
    if (videoDesktop) {
      videoDesktop.muted = !videoDesktop.muted;
    }
    event.target.classList.toggle('home-video--video-desktop-btn-sound-on');
  });

  if (videoMobile && videoDesktop) {
    timer = window.setInterval(() => {
      // if (videoDesktop.readyState !== 4 && videoMobile.readyState !== 4) {
      //   return;
      // }

      videoMobile.playsInline = true;
      videoDesktop.playsInline = true;
      videoMobile.setAttribute('playsinline', true);
      videoDesktop.setAttribute('playsinline', true);

      resizeManage();

      const foregroundAttr = (homeVideo.dataset.foreground === 'true');
      const foregroundBarAttr = (homeVideo.dataset.foregroundBar === 'true');

      document.body.classList.toggle('white-foreground', foregroundAttr);
      document.body.classList.toggle('white-foreground-bar', foregroundBarAttr);
      window.clearInterval(timer);
    }, 2000);
  }
}
