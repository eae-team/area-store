import { init } from 'aos';
import {on} from 'delegated-events';

export class Selector {
  constructor(element, initialValue) {
    this.el = element;
    this.el.id = `selector_${Math.round(Math.random() * 1000000)}`;
    this.showEl = element.querySelector('[data-action="show"]');
    this.optionsEl = element.querySelector('[data-role="options"]');
    if (initialValue) {
      this.setValue(initialValue);
    } else {
      this.value = this.optionsEl.firstElementChild.dataset.value;
    }
    on('click', `#${this.el.id} [data-action="select"]`, (event) => {
      event.preventDefault();
      this.optionsEl.classList.toggle('show');
      const selectedValue = event.target.dataset.value;
      if (selectedValue === this.value) {
        return;
      }
      this.setValue(selectedValue);

      const customEvent = new CustomEvent('selector-change', {detail: selectedValue});
      this.el.dispatchEvent(customEvent);
    });

    on('click', `#${this.el.id} [data-action="show"]`, (event) => {
      event.preventDefault();
      const selectFilter = event.target.parentElement.nextElementSibling.children[0];
      selectFilter.classList.toggle('show');
    });

    on('click', 'body', (event) => {
      if (!event.target.closest(`#${this.el.id}`)) {
        this.optionsEl.classList.toggle('show', false);
      }
    });
  }

  setValue(value) {
    const selectedEl = this.el.querySelector(`[data-value=${value}]`);
    this.value = value;
    this.optionsEl.prepend(selectedEl);
  }
}
