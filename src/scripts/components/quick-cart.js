import {on} from 'delegated-events';
import {CartBase} from './cart-base';
import {toggleCart, closeCart, updateCart} from '../components/cart-actions';

export class QuickCart extends CartBase {
  constructor(cartEl) {
    super(cartEl, 'quick-cart');

    on('click', '[data-action="open-cart"]', (event) => {
      event.preventDefault();
      toggleCart();
    });

    on('click', `#${this.el.id} [data-action="close"]`, (event) => {
      event.preventDefault();
      closeCart();
    });

    updateCart();
  }
}
