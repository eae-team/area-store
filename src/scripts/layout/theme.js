// import Cookies from 'js-cookie';
import 'lazysizes/plugins/object-fit/ls.object-fit';
import 'lazysizes/plugins/parent-fit/ls.parent-fit';
import 'lazysizes/plugins/rias/ls.rias';
import 'lazysizes/plugins/bgset/ls.bgset';
import 'lazysizes';
import 'lazysizes/plugins/respimg/ls.respimg';
import AOS from 'aos';

import '../../styles/theme.scss';
import '../../styles/theme.scss.liquid';
import {on} from 'delegated-events';

import {focusHash, bindInPageLinks} from '@shopify/theme-a11y';
import '../snippets/navigation';
import '../sections/header';
import {QuickCart} from '../components/quick-cart';

// Common a11y fixes
focusHash();
bindInPageLinks();

AOS.init({
  duration: 900,
  easing: 'ease-out-cubic',
  delay: 300,
  once: true,
});

// First we get the viewport height and we multiple it by 1% to get a value for a vh unit
let vh = window.innerHeight * 0.01;
// Then we set the value in the --vh custom property to the root of the document
document.documentElement.style.setProperty('--vh', `${vh}px`);

// We listen to the resize event
window.addEventListener('resize', () => {
  // We execute the same script as before
  vh = window.innerHeight * 0.01;
  document.documentElement.style.setProperty('--vh', `${vh}px`);
});

/*
checkCookieNotice();

function checkCookieNotice() {
  if (Cookies.get('cookie-accept')) {
    return;
  }
  setTimeout(() => {
    document.querySelector('[data-role="cookie-notice"]').classList.add('cookie__bar--show');
  }, 3000);
}

document.querySelector('[data-action="dismiss-cookie-notice"]').addEventListener('click', (event) => {
  event.preventDefault();
  Cookies.set('cookie-accept', true);
  document.querySelector('[data-role="cookie-notice"]').classList.remove('cookie__bar--show');
});
*/
window.addEventListener('scroll', (event) => {
  document.querySelector('body').classList.toggle('js-is-scrolled', window.scrollY > 10);
});

window.quickCart = new QuickCart(document.querySelector('[data-role="quick-cart"]'));

on('click', '[data-action="top"]', (event) => {
  event.preventDefault();
  window.scroll({
    behavior: 'smooth',
    left: 0,
    top: 0,
  });
});

on('click', '[data-action="history-back"]', (event) => {
  event.preventDefault();
  window.history.back();
});

on('click', '[data-switch-lang]', (event) => {
  event.preventDefault();
  const targetLang = event.target.dataset.switchLang;
  if (langify && langify.helper && langify.helper.localizationRedirect) {
    langify.helper.localizationRedirect('language_code', targetLang);
  } else if (targetLang === 'pt-PT') {
    window.location = '/';
  } else {
    window.location = `/${targetLang}`;
  }
});
