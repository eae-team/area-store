/* eslint-disable promise/catch-or-return */
import inView from 'in-view';
import axios from 'axios';
import {Selector} from './selector';
import {getCurrentBreak} from './utils';

// Elements
const collectionEl = document.querySelector('[data-collection-handle]');
const productsHolderEl = document.querySelector('[data-role="collection-holder"]');
const sortEls = document.querySelectorAll('[data-role="sort-select"]');
const filterEls = document.querySelectorAll('[data-role="filter-select"]');
const syncValue = document.querySelectorAll('[data-value]');
const loadMoreEl = document.querySelector('[data-action="load-more"]');

// Constants
const itemsPerPage = 12;
const collectionLayout = collectionEl.dataset.layout;
// Sizing parameters for page height calculations
const sizingParams = {
  regular: {
    small: {
      // grid product cols for mobile
      columns: 2,
      // H / W ratio for product - mobile
      ratio: 1,
      // fixed height for product info
      fixedHeight: 71,
    },
    medium: {
      columns: 2,
      ratio: 1,
      fixedHeight: 71,
    },
    large: {
      columns: 3,
      ratio: 1,
      fixedHeight: 86,
    },
  },
  grid1mobile: {
    small: {
      columns: 1,
      ratio: 1,
      fixedHeight: 71,
    },
    medium: {
      columns: 2,
      ratio: 1,
      fixedHeight: 71,
    },
    large: {
      columns: 3,
      ratio: 1,
      fixedHeight: 86,
    },
  },
  horizontal: {
    small: {
      columns: 1,
      ratio: 9 / 16,
      fixedHeight: 71,
    },
    medium: {
      columns: 2,
      ratio: 9 / 16,
      fixedHeight: 71,
    },
    large: {
      columns: 3,
      ratio: 9 / 16,
      fixedHeight: 86,
    },
  },
  vertical: {
    small: {
      columns: 2,
      ratio: 12 / 9,
      fixedHeight: 71,
    },
    medium: {
      columns: 2,
      ratio: 12 / 9,
      fixedHeight: 71,
    },
    large: {
      columns: 4,
      ratio: 12 / 9,
      fixedHeight: 86,
    },
  },
};
const currentCollection = collectionEl.dataset.collectionHandle;
let filterBy = collectionEl.dataset.tags || 'all';
let sortBy = collectionEl.dataset.sortBy || 'default';
const searchBy = collectionEl.dataset.searchBy || null;
let fetching = false;

// page
let currentPage = document.querySelectorAll('[data-page]').length;
let totalPages = Number(collectionEl.dataset.totalPages);

sortEls.forEach((el) => {
  new Selector(el, sortBy);
  el.addEventListener('selector-change', (event) => {
    const eventValue = event.detail;
    syncBothSelectors(eventValue);
    sortBy = eventValue;
    fetchProducts(1);
  });
});

filterEls.forEach((el) => {
  new Selector(el, filterBy);
  el.addEventListener('selector-change', (event) => {
    const eventValue = event.detail;
    syncBothSelectors(eventValue);
    filterBy = eventValue;
    fetchProducts(1);
  });
});

window.addEventListener('scroll', () => {
  checkScroll();
});

// Ensures visible page is loaded
window.setTimeout(() => {
  checkScroll();
}, 100);

if (loadMoreEl) {
  loadMoreEl.addEventListener('click', (event) => {
    event.preventDefault();
    fetchProducts(currentPage + 1);
  });
}

function checkScroll() {
  if (fetching || currentPage >= totalPages) {
    return;
  }
  const pageHeight = window.innerHeight;
  const offset = pageHeight / 2;
  const scrollTop = window.pageYOffset;

  if (document.body.offsetHeight - offset <= scrollTop + pageHeight) {
    fetchProducts(currentPage + 1);
  }
}

function syncBothSelectors(dataValue) {
  syncValue.forEach((el) => {
    const parent = el.parentElement;
    if (dataValue === el.getAttribute('data-value')) {
      parent.prepend(el);
    }
  });
}

function calcPageHeight() {
  const pageWidth = productsHolderEl.offsetWidth;
  const mediaBreak = getCurrentBreak();
  const sizing = sizingParams[collectionLayout][mediaBreak];
  const lines = itemsPerPage / sizing.columns;
  const pageHeight = ((pageWidth / sizing.columns * sizing.ratio) + sizing.fixedHeight) * lines;
  document.querySelectorAll('[data-page]').forEach((pageEl) => {
    if (pageEl.dataset.lastPage === '1') {
      return;
    }
    pageEl.style.height = `${pageHeight}px`;
  });
}

calcPageHeight();

window.addEventListener('resize', () => {
  calcPageHeight();
});

window.setTimeout(() => {
  inView('[data-page]').on('enter', (el) => loadPage(el));
}, 100);

function loadPage(pageEl) {
  if (pageEl.dataset.pageLoaded) {
    return;
  }
  const {page} = pageEl.dataset;
  pageEl.dataset.pageLoaded = 1;
  pageEl.classList.add('js-loading-page');

  let path = '';
  if (window.Shopify.locale !== 'pt-PT') {
    path = `/${window.Shopify.locale}`;
  }

  path += currentCollection === 'search' ? '/search' : `/collections/${currentCollection}`;
  if (filterBy !== 'all') {
    path += `/${filterBy}`;
  }
  path += `?page=${page}`;
  if (sortBy !== 'default') {
    path += `&sort_by=${sortBy}`;
  }
  if (searchBy) {
    path += `&q=${searchBy}&type=product`;
  }

  axios.get(`${path}&view=json`)
    .then((result) => {
      const products = result.data.body;
      if (page === 1) {
        productsHolderEl.innerHTML = '';
      }
      pageEl.innerHTML = products;
      return true;
    })
    .catch((error) => {
      return error;
    })
    .then(() => {
      pageEl.classList.remove('js-loading-page');
      return true;
    });
}

function fetchProducts(page) {
  if (fetching) {
    return;
  }
  fetching = true;

  if (page === 1) {
    productsHolderEl.classList.add('js-resetting');
  }

  let path = '';
  if (window.Shopify.locale !== 'pt-PT') {
    path = `/${window.Shopify.locale}`;
  }

  path += currentCollection === 'search' ? '/search' : `/collections/${currentCollection}`;
  if (filterBy !== 'all') {
    path += `/${filterBy}`;
  }
  path += `?page=${page}`;
  if (sortBy !== 'default') {
    path += `&sort_by=${sortBy}`;
  }
  if (searchBy) {
    path += `&q=${searchBy}&type=product`;
  }

  axios.get(`${path}&view=json`)
    .then((result) => {
      const products = result.data.body;

      if (page === 1) {
        productsHolderEl.innerHTML = '';
      }

      currentPage = page;
      totalPages = result.data.pageCount;

      const pageEl = document.createElement('div');
      pageEl.classList.add('collection__page');
      pageEl.dataset.page = currentPage;
      if (totalPages === currentPage) {
        pageEl.dataset.lastPage = '1';
      }
      pageEl.innerHTML += products;
      productsHolderEl.appendChild(pageEl);
      window.history.replaceState('', '', path);
      window.setTimeout(() => {
        checkScroll();
      }, 100);
      return true;
    })
    .catch((error) => {
      return error;
    })
    .then(() => {
      fetching = false;
      productsHolderEl.classList.remove('js-resetting');
      return true;
    });
}
