export function blockContent() {
  document.body.classList.add('_block-content');
}

export function unblockContent() {
  document.body.classList.remove('_block-content');
}

export const breaks = {
  small: 750,
  medium: 990,
  large: 1400,
};

export const collectionGridBreaks = {
  small: 0,
  medium: 750,
  large: 990,
};

export function addForegroundClass(data) {
  // eslint-disable-next-line shopify/prefer-early-return
  data.slides.forEach((el) => {
    if (el.classList.contains('swiper-slide-active')) {
      const foregroundAttr = (el.dataset.foreground === 'true');
      const foregroundBarAttr = (el.dataset.foregroundBar === 'true');

      document.body.classList.toggle('white-foreground', foregroundAttr);
      document.body.classList.toggle('white-foreground-bar', foregroundBarAttr);
      document.body.classList.toggle('js-split-slide', foregroundBarAttr !== foregroundAttr);
    }
  });
}

export function getCurrentBreak() {
  const windowWidth = window.innerWidth;
  let mediaBreak = 'small';
  Object.keys(collectionGridBreaks).forEach((step) => {
    if (collectionGridBreaks[step] <= windowWidth) {
      mediaBreak = step;
    }
  });
  return mediaBreak;
}
