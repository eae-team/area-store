import {on} from 'delegated-events';
import {blockContent, unBlockContent, breaks} from '../components/utils';

// Element
const navEl = document.querySelector('nav');
const header = document.querySelector('header');
const menuOpenEl = document.querySelector('[data-action="menu-open"]');
const menuCloseEl = document.querySelector('[data-action="menu-close"]');
const headerSearchEl = document.querySelector('[data-role="header-search"]');
const headerSearchInputEl = headerSearchEl.querySelector('input[name="q"]');

// click to open
on('click', '[data-action="menu-open"]', (event) => {
  event.preventDefault();
  menuOpenClose();
  // blockContent();
});

// click to close
on('click', '[data-action="menu-close"]', (event) => {
  event.preventDefault();
  menuOpenClose();
  // unBlockContent();
});

// menu function
function menuOpenClose() {
  header.classList.toggle('header--opened');
  navEl.classList.toggle('navigation--opened');
  menuOpenEl.classList.toggle('hidden');
  menuCloseEl.classList.toggle('show');
}

// Header search logic
function openHeaderSearch() {
  headerSearchEl.classList.add('js-open');
}

function closeHeaderSearch() {
  headerSearchEl.classList.remove('js-open');
}

headerSearchInputEl.addEventListener('focus', (event) => {
  openHeaderSearch();
});

headerSearchInputEl.addEventListener('blur', (event) => {
  if (!headerSearchInputEl.value) {
    closeHeaderSearch();
  }
});

// remove menu open classes on desktop
window.addEventListener('resize', (event) => {
  if (window.matchMedia(`(min-width: ${breaks.medium}px)`).matches) {
    header.classList.remove('header--opened');
    navEl.classList.remove('navigation--opened');
    menuOpenEl.classList.remove('hidden');
    menuCloseEl.classList.remove('show');
  }
});
