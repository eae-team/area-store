/**
 * Product Template Script
 * ------------------------------------------------------------------------------
 * A file that contains scripts highly couple code to the Product template.
 *
 * @namespace product
 */
import {getUrlWithVariant, ProductForm} from '@shopify/theme-product-form';
import {formatMoney} from '@shopify/theme-currency';
import {register} from '@shopify/theme-sections';
import {forceFocus} from '@shopify/theme-a11y';
import axios from 'axios';
import {addToCart, openCart, closeCart} from '../components/cart-actions';
import { allSettled } from 'q';

const classes = {
  hide: 'hide',
};

const keyboardKeys = {
  ENTER: 13,
};

const selectors = {
  submitButton: '[data-submit-button]',
  submitButtonText: '[data-submit-button-text]',
  comparePrice: '[data-compare-price]',
  comparePriceText: '[data-compare-text]',
  priceWrapper: '[data-price-wrapper]',
  imageWrapper: '[data-product-image-wrapper]',
  visibleImageWrapper: `[data-product-image-wrapper]:not(.${classes.hide})`,
  imageWrapperById: (id) => `${selectors.imageWrapper}[data-image-id='${id}']`,
  productForm: '[data-product-form]',
  productPrice: '[data-product-price]',
  thumbnail: '[data-product-single-thumbnail]',
  thumbnailById: (id) => `[data-thumbnail-id='${id}']`,
  thumbnailActive: '[data-product-single-thumbnail][aria-current]',
  variantToggles: '[data-action="open-variant"]',
  variantAddBtns: '[data-action="add-variant"]',
  variantSelect: '[data-role="variant-select"]',
  backInStockForm: '[data-role="back-in-stock-form"]',
  notificationMessage: '[data-role="notif-message"]',
};

register('product', {
  async onLoad() {
    const productFormElement = document.querySelector(selectors.productForm);

    this.product = await this.getProductJson(
      productFormElement.dataset.productHandle,
    );
    this.productForm = new ProductForm(productFormElement, this.product, {
      onOptionChange: this.onFormOptionChange.bind(this),
      onFormSubmit: this.onFormSubmit.bind(this),
    });

    this.variantSelect = this.container.querySelector('[data-role="variant-select"]');

    this.onThumbnailClick = this.onThumbnailClick.bind(this);
    this.onThumbnailKeyup = this.onThumbnailKeyup.bind(this);
    this.onVariantToggle = this.onVariantToggle.bind(this);
    this.onAddVariant = this.onAddVariant.bind(this);

    this.container.querySelectorAll(selectors.variantToggles).forEach((toggleEl) => {
      toggleEl.addEventListener('click', this.onVariantToggle);
    });

    this.container.querySelectorAll(selectors.variantAddBtns).forEach((addEl) => {
      addEl.addEventListener('click', this.onAddVariant);
    });

    this.container.addEventListener('click', this.onThumbnailClick);
    this.container.addEventListener('keyup', this.onThumbnailKeyup);
    const backInStockForm = document.querySelector(selectors.backInStockForm);
    if (backInStockForm) {
      backInStockForm.addEventListener('submit', this.submitBackInStock);
    }
  },

  onUnload() {
    this.productForm.destroy();
    this.removeEventListener('click', this.onThumbnailClick);
    this.removeEventListener('keyup', this.onThumbnailKeyup);
  },

  getProductJson(handle) {
    return fetch(`/products/${handle}.js`).then((response) => {
      return response.json();
    });
  },

  onFormOptionChange(event) {
    const variant = event.dataset.variant;

    this.renderImages(variant);
    this.renderPrice(variant);
    this.renderComparePrice(variant);
    this.renderSubmitButton(variant);

    this.updateBrowserHistory(variant);
  },

  onFormSubmit(event) {
    event.preventDefault();
    const quantity = this.productForm.quantity();
    const variantId = this.productForm.variant().id || this.product.id;

    addToCart(variantId, quantity)
      .then((isSuccess) => {
        return isSuccess;
      })
      .catch(() => {
        return false;
      });
  },

  onThumbnailClick(event) {
    const thumbnail = event.target.closest(selectors.thumbnail);

    if (!thumbnail) {
      return;
    }

    event.preventDefault();

    this.renderFeaturedImage(thumbnail.dataset.thumbnailId);
    this.renderActiveThumbnail(thumbnail.dataset.thumbnailId);
  },

  onThumbnailKeyup(event) {
    if (
      event.keyCode !== keyboardKeys.ENTER ||
      !event.target.closest(selectors.thumbnail)
    ) {
      return;
    }

    const visibleFeaturedImageWrapper = this.container.querySelector(
      selectors.visibleImageWrapper,
    );

    forceFocus(visibleFeaturedImageWrapper);
  },

  onVariantToggle(event) {
    event.preventDefault();
    const activeVariant = this.container.querySelector('[data-role="variant"].js-active');
    const targetVariant = event.target.closest('[data-role="variant"]');
    if (activeVariant) {
      activeVariant.classList.remove('js-active');
    }
    if (activeVariant !== targetVariant) {
      targetVariant.classList.add('js-active');
    }
  },

  onAddVariant(event) {
    event.preventDefault();
    const variantElement = event.target.closest('[data-role="variant"]');
    const quantityElement = this.container.querySelector('[name="quantity"]');
    const submitElement = this.container.querySelector(selectors.submitButton);
    const variantQtd = variantElement.querySelector('[data-role="variant-quantity"]').value;
    quantityElement.value = variantQtd;
    this.variantSelect.value = event.target.dataset.option;
    if ('createEvent' in document) {
      const evt = document.createEvent('HTMLEvents');
      evt.initEvent('change', false, true);
      this.variantSelect.dispatchEvent(evt);
    } else {
      this.variantSelect.fireEvent('onchange');
    }
    submitElement.click();
  },

  renderSubmitButton(variant) {
    const submitButton = this.container.querySelector(selectors.submitButton);
    const submitButtonText = this.container.querySelector(
      selectors.submitButtonText,
    );

    if (!variant) {
      submitButton.disabled = true;
      submitButtonText.innerText = theme.strings.unavailable;
    } else if (variant.available) {
      submitButton.disabled = false;
      submitButtonText.innerText = theme.strings.addToCart;
    } else {
      submitButton.disabled = true;
      submitButtonText.innerText = theme.strings.soldOut;
    }
  },

  renderImages(variant) {
    if (!variant || variant.featured_image === null) {
      return;
    }

    this.renderFeaturedImage(variant.featured_image.id);
    this.renderActiveThumbnail(variant.featured_image.id);
  },

  renderPrice(variant) {
    const priceElement = this.container.querySelector(selectors.productPrice);
    const priceWrapperElement = this.container.querySelector(
      selectors.priceWrapper,
    );

    priceWrapperElement.classList.toggle(classes.hide, !variant);

    if (variant) {
      priceElement.innerText = formatMoney(variant.price, theme.moneyFormat);
    }
  },

  renderComparePrice(variant) {
    if (!variant) {
      return;
    }

    const comparePriceElement = this.container.querySelector(
      selectors.comparePrice,
    );
    const compareTextElement = this.container.querySelector(
      selectors.comparePriceText,
    );

    if (!comparePriceElement || !compareTextElement) {
      return;
    }

    if (variant.compare_at_price > variant.price) {
      comparePriceElement.innerText = formatMoney(
        variant.compare_at_price,
        theme.moneyFormat,
      );
      compareTextElement.classList.remove(classes.hide);
      comparePriceElement.classList.remove(classes.hide);
    } else {
      comparePriceElement.innerText = '';
      compareTextElement.classList.add(classes.hide);
      comparePriceElement.classList.add(classes.hide);
    }
  },

  renderActiveThumbnail(id) {
    const activeThumbnail = this.container.querySelector(
      selectors.thumbnailById(id),
    );
    const inactiveThumbnail = this.container.querySelector(
      selectors.thumbnailActive,
    );

    if (activeThumbnail === inactiveThumbnail) {
      return;
    }

    inactiveThumbnail.removeAttribute('aria-current');
    activeThumbnail.setAttribute('aria-current', true);
  },

  renderFeaturedImage(id) {
    const activeImage = this.container.querySelector(
      selectors.visibleImageWrapper,
    );
    const inactiveImage = this.container.querySelector(
      selectors.imageWrapperById(id),
    );

    activeImage.classList.add(classes.hide);
    inactiveImage.classList.remove(classes.hide);
  },

  updateBrowserHistory(variant) {
    const enableHistoryState = this.productForm.element.dataset
      .enableHistoryState;

    if (!variant || enableHistoryState !== 'true') {
      return;
    }

    const url = getUrlWithVariant(window.location.href, variant.id);
    window.history.replaceState({path: url}, '', url);
  },

  submitBackInStock(event) {
    event.preventDefault();
    const variantId = this.dataset.variant;
    const requestUrl = this.action;
    const email = this.querySelector('input[type="email"]').value;
    const lang = document.documentElement.getAttribute('lang');
    if (!email) {
      console.log('Email required!');
      return;
    }
    this.classList.add('_submitting');
    this.classList.remove('_error');
    // eslint-disable-next-line promise/catch-or-return
    axios.post(requestUrl, {
      variant: variantId,
      email,
      platform: 'shopify',
      lang,
    })
      .then((result) => {
        if (result.data.success) {
          this.classList.add('_success');
          document.querySelector('[data-role="notif-message"]').style.display = 'none';
        } else {
          this.classList.add('_error');
        }
        return true;
      })
      .catch(() => {
        this.classList.add('_error');
        return false;
      })
      .then(() => {
        this.classList.remove('_submitting');
        return true;
      });
  },
});

