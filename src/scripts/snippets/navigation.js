import {on} from 'delegated-events';
import {breaks} from '../components/utils';

// First ul element default value
const submenuEls = document.querySelectorAll('[data-menulevel="1"]');

// function to close menu on Desktop
window.addEventListener('DOMContentLoaded', () => {
  if (!window.matchMedia(`(min-width: ${breaks.medium}px)`).matches) {
    submenuEls.forEach((el) => {
      el.classList.add('_open');
    });
  }
});

// flag to avoid timeouts overlap
let menuWorking = false;

const activeMenu = [
  null,
  document.querySelector('[data-menulevel="1"]._open'),
  document.querySelector('[data-menulevel="2"]._open'),
];

function openSubmenu(submenuEl) {
  if (menuWorking) {
    return;
  }
  const menuLevel = Number(submenuEl.dataset.menulevel);
  if (activeMenu[menuLevel] && activeMenu[menuLevel] !== submenuEl) {
    closeSubmenu(activeMenu[menuLevel]);
  }
  menuWorking = true;
  const ulEl = submenuEl.querySelector(':scope > ul');
  const targetHeight = ulEl.offsetHeight;
  const submenuItemEls = submenuEl.querySelectorAll(':scope > ul > li');
  submenuEl.style.height = `${targetHeight}px`;
  submenuEl.classList.add('_open');
  activeMenu[menuLevel] = submenuEl;
  let delay = 100;
  submenuItemEls.forEach((itemEl) => {
    window.setTimeout(() => {
      itemEl.style.opacity = 1;
    }, delay);
    delay += 100;
  });
  window.setTimeout(() => {
    submenuEl.style.height = 'auto';
    menuWorking = false;
  }, Math.max(550, delay));
  window.setTimeout(() => {
    navScrollCalc();
  }, 100);
}

function closeSubmenu(submenuEl) {
  if (menuWorking) {
    return;
  }
  menuWorking = true;
  const ulEl = submenuEl.querySelector(':scope > ul');
  const submenuItemEls = submenuEl.querySelectorAll(':scope > ul > li');
  const menuLevel = Number(submenuEl.dataset.menulevel);
  submenuEl.style.height = `${ulEl.offsetHeight}px`;
  window.setTimeout(() => {
    submenuEl.style.height = '0';
  }, 100);

  submenuEl.classList.remove('_open');
  if (activeMenu[menuLevel] === submenuEl) {
    activeMenu[menuLevel] = null;
  }

  let delay = submenuItemEls.length * 100;
  window.setTimeout(() => {
    menuWorking = false;
  }, Math.max(550, delay));
  submenuItemEls.forEach((itemEl) => {
    delay -= 100;
    window.setTimeout(() => {
      itemEl.style.opacity = 0;
    }, delay);
  });
  navScrollCalc();
}

function toggleSubmenu(submenuEl) {
  if (submenuEl.classList.contains('_open')) {
    closeSubmenu(submenuEl);
  } else {
    openSubmenu(submenuEl);
  }
}

on('click', '[data-role="submenu-trigger"] > [data-role="link"]', (event) => {
  event.preventDefault();
  toggleSubmenu(event.target.parentElement.querySelector('[data-role="submenu"]'));
});

// Scroll test
const navScrollElement = document.querySelector('[data-role="navigation"] > ul');
function navScrollCalc() {
  const availHeight = navScrollElement.offsetHeight - 60;
  const scrollTop = navScrollElement.scrollTop;
  const toVanishTop = Math.ceil(scrollTop / 16.8);
  const toVanishBottom = toVanishTop + Math.ceil((availHeight) / 16.8) - 5;
  // console.log(availHeight, toVanishTop, toVanishBottom);
  const activeMenuItems = navScrollElement.querySelectorAll('[data-level="1"] > li, ._open > [data-level="2"] > li, ._open > [data-level="3"] > li');
  activeMenuItems.forEach((item, idx) => {
    let opacity = 100;
    if (idx < toVanishTop) {
      opacity = (toVanishTop - idx) > 3 ? 0 : 100 - ((toVanishTop - idx) * 25);
    } else if (idx > toVanishBottom) {
      opacity = (idx - toVanishBottom) > 3 ? 0 : 100 - ((idx - toVanishBottom) * 25);
    }
    const menuLink = item.querySelector(':scope > a');
    if (menuLink) {
      menuLink.style.opacity = `${opacity}%`;
    }
  });
}
navScrollElement.addEventListener('scroll', () => {
  navScrollCalc();
});
window.addEventListener('resize', () => {
  navScrollCalc();
});
navScrollCalc();
