import {load} from '@shopify/theme-sections';
import '../sections/product';
import Swiper from 'swiper/bundle';

// Element
const imgEl = document.querySelector('[data-role="product-slider"]');
const thumbEl = document.querySelector('[data-role="thumb-slider"]');
const qtdInputs = document.querySelectorAll('[data-role="qtd-input"]');
let swiperThumb = null;

/*
if (thumbEl) {
  swiperThumb = new Swiper(thumbEl, {
    allowTouchMove: false,
    speed: 1000,
    observer: true,
    spaceBetween: 5,
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
    noSwiping: false,
  });
}
*/

let paginationConf = null;
paginationConf = {el: '.swiper-pagination'};

const swiperProduct = new Swiper(imgEl, {
  loop: true,
  slidesPerView: 1,
  speed: 1000,
  centeredSlides: true,
  effect: 'fade',
  fadeEffect: {
    crossFade: true,
  },
  pagination: paginationConf,
});

qtdInputs.forEach((qtdInput) => {
  qtdInput.querySelector('[data-action="decrease"]').addEventListener('click', () => {
    const targetInput = qtdInput.querySelector('input');
    const currentVal = Number(targetInput.getAttribute('value'));
    if (currentVal > 1) {
      targetInput.setAttribute('value', currentVal - 1);
    }
  });
  qtdInput.querySelector('[data-action="increase"]').addEventListener('click', () => {
    const targetInput = qtdInput.querySelector('input');
    const currentVal = targetInput.getAttribute('value');
    targetInput.setAttribute('value', Number(currentVal) + 1);
  });
});

document.querySelectorAll('[data-role="thumb"]').forEach((el) => {
  el.addEventListener('click', () => {
    swiperProduct.slideTo(Number(el.dataset.swiperSlideIndex) + 1);
  });
});

load('*');
