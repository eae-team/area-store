import * as cart from '@shopify/theme-cart';
import axios from 'axios';
import {blockContent, unblockContent} from '../components/utils';

export function openCart() {
  document.body.classList.add('js-cart-open');
  // blockContent();
}

export function closeCart() {
  document.body.classList.remove('js-cart-open');
  // unblockContent();
}

export function toggleCart() {
  document.body.classList.toggle('js-cart-open');
  // blockContent();
}

export function removeCartLine(lineKey) {
  return cart.removeItem(lineKey).then(() => {
    return updateCart();
  });
}

export function updateCart() {
  document.body.classList.add('js-cart-loading');
  let uriPrefix = '';
  if (window.Shopify.locale !== 'pt-PT') {
    uriPrefix = `/${window.Shopify.locale}`;
  }
  // eslint-disable-next-line promise/catch-or-return
  axios.get(`${uriPrefix}/cart?view=json`)
    // eslint-disable-next-line promise/always-return
    .then((result) => {
      const cartData = result.data;
      window.quickCart.updateCart(cartData);
      if (window.pagecart) {
        window.pagecart.updateCart(cartData);
      }
      document.querySelectorAll('[data-role="cart-qtd"]').forEach((element) => {
        element.innerHTML = cartData.cartItemCount;
      });
      return true;
    })
    .catch(() => {
      console.log('Error updating cart');
    })
    // eslint-disable-next-line promise/always-return
    .then(() => {
      document.body.classList.remove('js-cart-loading');
    });
}

export function addToCart(variantId, quantity, properties) {
  document.body.classList.add('js-cart-loading');
  return cart
    .addItem(Number(variantId), {quantity, properties})
    .then(() => {
      updateCart();
      openCart();
      return quantity;
    })
    .catch((response) => {
      document.body.classList.remove('js-cart-loading');
      return response.status === 422 ? 0 : -1;
    });
}

export function updateItem(lineKey, quantity) {
  document.body.classList.add('js-cart-loading');
  return cart
    .updateItem(lineKey, {quantity: Number(quantity)})
    .then(() => {
      updateCart();
      return quantity;
    })
    .catch((response) => {
      return response.status === 422 ? 0 : -1;
    });
}
