import Swiper from 'swiper/bundle';
import {addForegroundClass, breaks} from '../components/utils';
// elements
const swiperHome = document.querySelector('[data-role="home-slider"]');
const mutedBtnEl = document.querySelector('[data-action="btn-muted"]');

const breakpointDesktop = window.matchMedia(`(min-width: ${breaks.medium}px)`);
let swiperInstanceDesktop;

// swiper instance
function swiper() {
  if (!swiperHome) {
    return;
  }

  const autoPlay = swiperHome.dataset.auto * 1000;
  let autoPlayOption = false;
  if (autoPlay) {
    autoPlayOption = {
      delay: autoPlay,
    };
  }

  swiperInstanceDesktop = new Swiper(swiperHome, {
    allowTouchMove: false,
    loop: true,
    speed: 1000,
    effect: 'fade',
    autoplay: autoPlayOption,
    preventClicks: false,
    fadeEffect: {
      crossFade: true,
    },
    navigation: {
      nextEl: '.slider__arrow-right',
      prevEl: '.slider__arrow-left',
    },
    on: {
      beforeInit(sw) {
        addForegroundClass(sw);
      },
      beforeTransitionStart(sw) {
        addForegroundClass(sw);
      },
      slideChange(sw) {
        checkSlide(sw);
      },
    },
  });
}

function checkSlide(sw) {
  if (!mutedBtnEl) {
    return;
  }
  const currentSlide = sw.slides[sw.activeIndex];

  if (currentSlide.classList.contains('slide-inner-video')) {
    mutedBtnEl.classList.add('home-slider--show');
    mutedBtnEl.addEventListener('click', () => mutedToogle(currentSlide));
  } else {
    mutedBtnEl.classList.remove('home-slider--show');
    mutedBtnEl.removeEventListener('click', mutedToogle);
    mutedAllVideos();
  }
}

function breakpointChecker() {
  if (breakpointDesktop.matches === false) {
    if (swiperInstanceDesktop !== undefined) {
      swiperInstanceDesktop.destroy(true, true);
    }
  } else if (breakpointDesktop.matches === true) {
    swiper();
  }
}

function mutedToogle(currentSlide) {
  // mutedBtnEl.classList.toggle('home-slider--muted-btn-on');
  const video = currentSlide.querySelector('video');
  video.muted = !video.muted;
}

function mutedAllVideos() {
  const videos = swiperHome.querySelectorAll('video');
  videos.forEach((element) => {
    element.muted = true;
  });
}

breakpointDesktop.addListener(breakpointChecker);
breakpointChecker();
