import Swiper from 'swiper/bundle';

const slider1 = document.querySelector('[data-role="slider1"]');
const slider2 = document.querySelector('[data-role="slider2"]');
const slideEls = document.querySelectorAll('[data-role="slide"]');

let swiperSlider1;
let swiperSlider2;

if (slider1) {
  swiperSlider1 = new Swiper(slider1, {
    spaceBetween: 5,
    slidesPerView: 'auto',
    speed: 1000,
    updateOnWindowResize: true,
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    pagination: {
      el: '.swiper-pagination',
      type: 'bullets',
    },
  });
}

if (slider2) {
  swiperSlider2 = new Swiper(slider2, {
    spaceBetween: 5,
    slidesPerView: 'auto',
    speed: 1000,
    updateOnWindowResize: true,
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    pagination: {
      el: '.swiper-pagination',
      type: 'bullets',
    },
  });
}

const resizeElements = () => {
  const viewportWidth = window.innerWidth * 0.8;

  let maxWidth = 0;
  let maxHeight = 0;

  slideEls.forEach((element) => {
    const width = parseInt(element.dataset.width, 10);
    const height = parseInt(element.dataset.height, 10);

    if (width > maxWidth) {
      maxWidth = width;
      maxHeight = height > 700 ? 700 : height;
    }
  });

  const targetHeight = viewportWidth * (maxHeight / maxWidth);

  slideEls.forEach((element) => {
    const width = parseInt(element.dataset.width, 10);
    const height = parseInt(element.dataset.height, 10);

    const newWidth = targetHeight * (width / height);
    const newHeight = targetHeight;

    element.style.width = `${newWidth}px`;
    element.style.height = `${newHeight}px`;
  });

  if (swiperSlider1) {
    swiperSlider1.update();
  }

  if (swiperSlider2) {
    swiperSlider2.update();
  }
};

window.addEventListener('DOMContentLoaded', resizeElements);
window.addEventListener('resize', resizeElements);
