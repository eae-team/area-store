/*
 * Controller for account main page
 */

const selectors = {
  introEl: '[data-role="intro"]',
  ordersEl: '[data-role="orders"]',
  ordersButton: '[data-action="orders]',
};

const introEl = document.querySelector(selectors.introEl);
const ordersEl = document.querySelector(selectors.ordersEl);

function checkHash() {
  if (window.location.hash === '#orders') {
    introEl.style.display = 'none';
    ordersEl.style.display = 'block';
  } else {
    introEl.style.display = 'block';
    ordersEl.style.display = 'none';
  }
}

window.addEventListener('hashchange', () => {
  checkHash();
});

checkHash();
